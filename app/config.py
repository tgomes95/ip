import os

SQLITE_DATABSE_URI_BASE = 'sqlite:///'
DATABASE_NAME           = 'database'

class Config:
    DEBUG                          = True
    TESTING                        = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY                     = 'VlWNX@&7:d5n4<r'
    HOST                           = '0.0.0.0'
    PORT                           = '8000'

class Development(Config):
    DEBUG                   = True
    SQLALCHEMY_DATABASE_URI = SQLITE_DATABSE_URI_BASE + DATABASE_NAME + '.db'

class Production(Config):
    DEBUG                   = False
    TESTING                 = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or SQLITE_DATABSE_URI_BASE + DATABASE_NAME + '.db'
