#!/usr/bin/env python

from flask import Flask
from flask import jsonify
from flask import render_template
from flask import request

import config

app = Flask(__name__)
app.config.from_object(config.Development)

@app.route('/')
@app.route('/home')
def home():
    ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    return render_template('home.html', ip=ip)

@app.route('/api')
def api():
    url = ''.join(request.base_url.split('://')[1:]) + '/ip'
    return render_template('api.html', url=url)

@app.route('/api/ip')
def api_ip():
    ip = '%s\n' % request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    return ip

@app.errorhandler(403)
def forbidden(error):
    return render_template('403.html'), 403

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

@app.errorhandler(405)
def method_not_allowed(error):
    return render_template('405.html'), 405

@app.errorhandler(500)
def internal_server_error(error):
    return render_template('500.html'), 500

if __name__ == '__main__':
    app.run(host=config.Development.HOST, port=config.Development.PORT)
